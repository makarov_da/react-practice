import express from 'express';
import path from 'path';
import cors from 'cors';
import handleRender from './handleRender';

const app = express();
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));
app.get('/*', handleRender);
// app.get('/*', (request, response) => {
//     response.sendFile(path.resolve(__dirname, 'public/index.html'));
// });

app.listen(3000, () => {
    console.log('Listening on port 3000! -> http://localhost:3000');
});