## Quick start

1. Clone this repo using `git clone https://bitbucket.org/makarov_da/react-practice.git`
2. Move to the appropriate directory: `cd react-practice`.
3. Run `npm install` in order to install all modules.
4. Run `npm start` in order to build project and start server.
5. Go to http://localhost:3000/
6. Use Code Quality Tools / ESLint in WebStorm, set ESLint package '\react-practice\node_modules\eslint'
7. Run `npm test` in order to start test with Jest