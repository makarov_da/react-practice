import React from 'react';
import { renderToString } from 'react-dom/server';

function renderFullPage() {
    return `
      <!doctype html>
      <html>
        <head>
          <meta charset=utf-8>
          <title>ReactJS practies</title>
        </head>
        <body>
            <div id="root"/>
            <script src="/bundle.js"></script>
        </body>
      </html>
  `;
}

function handleRender(req, res) {
    res.send(renderFullPage());
}

export default handleRender;
