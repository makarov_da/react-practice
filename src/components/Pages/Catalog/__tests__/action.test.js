import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import { searchResponseMock, searchPersonMock } from '../__mocks__/mocks';
import * as actions from '../actions/index';
import * as actionTypes from '../actions/actionTypes';

describe('Catalog actions', () => {
    it('should create an action to setSearchType', () => {
        const searchType = 'director'
        const expectedAction = {
            type: actionTypes.TYPE_SEARCH,
            searchType,
        };
        expect(actions.setSearchType(searchType)).toEqual(expectedAction);
    });

    it('should create an action to initializeCatalog', () => {
        const expectedAction = {
            type: actionTypes.INITIALIZE,
        };
        expect(actions.initializeCatalog()).toEqual(expectedAction);
    });

    it('should create an action to setSortType', () => {
        const sortType = 'rating';
        const expectedAction = {
            type: actionTypes.TYPE_SORT,
            sortType,
        };
        expect(actions.setSortType(sortType)).toEqual(expectedAction);
    });

    it('should create an action to setSearchQuery', () => {
        const searchQuery = 'some search text';
        const expectedAction = {
            type: actionTypes.SEARCH_QUERY,
            searchQuery,
        };
        expect(actions.setSearchQuery(searchQuery)).toEqual(expectedAction);
    });
});

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Catalog async actions', () => {

    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    it('async searchFilms', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: searchResponseMock,
            });
        });

        const expectedActions = [
            {
                results: searchResponseMock.results,
                page: 1,
                total_results: 12,
                total_pages: 1,
                searchQuery: "KILL BILL",
                searchType: "title",
                sortType: "rating",
                type: actionTypes.FETCH_RECORDS_SUCCESS,
            },
        ];

        const store = mockStore({
                searchQuery: '',
                searchType: 'title',
                sortType: 'rating',
                results: [],
                page: 0,
                total_pages: 0,
                total_results: 0,
        });

        return store.dispatch(actions.searchFilms('KILL BILL', { searchType:'title', sortType:'rating' }))
            .then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });

    });


    it('async getGenre', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                genres: [],
            });
        });

        const store = mockStore({
            genres: [],
        });

        return store.dispatch(actions.getGenre())
            .then(() => {
            expect([]).not.toBe([]);
        });
    });

});