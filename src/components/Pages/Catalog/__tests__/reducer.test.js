import Immutable from 'immutable';
import { catalogPageReducer } from '../reducer';
import * as actionTypes from '../actions/actionTypes';

describe('catalogPageReducer reducer', () => {

    it('should return the initial state', () => {
        expect(catalogPageReducer(undefined, {})).toEqual(Immutable.fromJS({
                initPageCatalog: false,
                searchType: 'title',
                sortType: 'rating',
                searchQuery: '',
                isLoading: false,
                results: [],
                genres: [],
                page: 0,
                total_pages: 0,
                total_results: 0,
            })
        )
    });

    it('should return searchQuery', () => {
        expect(catalogPageReducer(undefined,{searchQuery: 'test', type: actionTypes.SEARCH_QUERY}))
            .toEqual(Immutable.fromJS({
                searchQuery: 'test',
                initPageCatalog: false,
                searchType: 'title',
                sortType: 'rating',
                isLoading: false,
                results: [],
                genres: [],
                page: 0,
                total_pages: 0,
                total_results: 0,
        }))
    });

    it('should return typeSearch', () => {
        expect(catalogPageReducer(undefined,{searchType: 'director', type: actionTypes.TYPE_SEARCH}))
            .toEqual(Immutable.fromJS({
                searchQuery: '',
                initPageCatalog: false,
                searchType: 'director',
                sortType: 'rating',
                isLoading: false,
                results: [],
                genres: [],
                page: 0,
                total_pages: 0,
                total_results: 0,
        }))
    });

    it('should return typeSort', () => {
        expect(catalogPageReducer(undefined,{sortType: 'release date', type: actionTypes.TYPE_SORT}))
            .toEqual(Immutable.fromJS({
                searchQuery: '',
                initPageCatalog: false,
                searchType: 'title',
                sortType: 'release date',
                isLoading: false,
                results: [],
                genres: [],
                page: 0,
                total_pages: 0,
                total_results: 0,
        }))
    });

    it('should return isLoading', () => {
        expect(catalogPageReducer(undefined,{isLoading: true, type: actionTypes.FETCH_RECORDS_LOADING}))
            .toEqual(Immutable.fromJS({
                searchQuery: '',
                initPageCatalog: false,
                searchType: 'title',
                sortType: 'rating',
                isLoading: true,
                results: [],
                genres: [],
                page: 0,
                total_pages: 0,
                total_results: 0,
        }))
    });

    it('should return genre', () => {
        expect(catalogPageReducer(undefined,{genres: [{id: 28, name: 'Action'}], isLoading: true, type: actionTypes.FETCH_GENRE_RECORDS_SUCCESS}))
            .toEqual(Immutable.fromJS({
                searchQuery: '',
                initPageCatalog: false,
                searchType: 'title',
                sortType: 'rating',
                isLoading: false,
                results: [],
                genres: [{id: 28, name: 'Action'}],
                page: 0,
                total_pages: 0,
                total_results: 0,
        }))
    });

    it('should return fetchData', () => {
        expect(catalogPageReducer(undefined,
            {
                searchType: 'testSearchType',
                sortType: 'testSortType',
                searchQuery: 'testSearchQuery',
                isLoading: false,
                results: [{key: 'value'}],
                page: 1,
                total_pages: 1,
                total_results: 10,
                type: actionTypes.FETCH_RECORDS_SUCCESS,
            }))
            .toEqual(Immutable.fromJS({
                initPageCatalog: false,
                genres: [],
                total_pages: 1,
                total_results: 10,

                searchType: 'testSearchType',
                sortType: 'testSortType',
                searchQuery: 'testSearchQuery',
                isLoading: false,
                results: [{key: 'value'}],
                page: 1,
        }))
    });


});