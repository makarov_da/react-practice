const prefix = '@@catalog/';

export const INITIALIZE = `${prefix}INIT`;

export const FETCH_RECORDS_LOADING = `${prefix}FETCH_START`;
export const FETCH_RECORDS_SUCCESS = `${prefix}MOVIES_FETCH_SUCCESS`;
export const FETCH_GENRE_RECORDS_SUCCESS = `${prefix}GENRE_FETCH_SUCCESS`;
export const FETCH_RECORDS_FAILED = `${prefix}FETCH_FAILED`;

export const NEXT_PAGE = `${prefix}NEXT_PAGE`;
export const PREV_PAGE = `${prefix}PREV_PAGE`;

export const GOTO_PAGE = `${prefix}GOTO_PAGE`;

export const SEARCH_QUERY = `${prefix}CHANGE_SEARCH_TEXT`;

export const TYPE_SORT = `${prefix}CHANGE_TYPE_SORT`;

export const SET_PAGE_SIZE = `${prefix}SET_PAGE_SIZE`;

export const TYPE_SEARCH = `${prefix}CHANGE_TYPE_SEARCH`;

export const SET_BRAND = `${prefix}SET_BRAND`;