import api from '../../../_commons/api';
import * as actionTypes from './actionTypes';

export const initializeCatalog = () => ({
    type: actionTypes.INITIALIZE,
});

export const setSearchType = type => ({
    type: actionTypes.TYPE_SEARCH,
    searchType: type,
});

export const setSortType = type => ({
    type: actionTypes.TYPE_SORT,
    sortType: type,
});

export const setSearchQuery = searchQuery => ({
    type: actionTypes.SEARCH_QUERY,
    searchQuery,
});

export const loadingFetch = () => ({
    type: actionTypes.FETCH_RECORDS_LOADING,
});


export const searchFilms = (searchQuery, searchSettings) => (dispatch) => {
    loadingFetch();
    return api.collectionSearch.list(searchQuery, searchSettings)
        .then((response) => {
            const {
                searchType,
                sortType,
            } = searchSettings;
            const { results, page, total_pages, total_results } = response.data;
            dispatch({
                type: actionTypes.FETCH_RECORDS_SUCCESS,
                searchType,
                searchQuery,
                sortType,
                results,
                page,
                total_pages,
                total_results,
            });
            return response;
        })
        .catch(({ error }) => {
        debugger
            dispatch({ type: actionTypes.FETCH_RECORDS_FAILED, error });
        });
};

export const getGenre = () => {
    return (dispatch) => {
        loadingFetch();
        return api.collectionSearch.genre()
            .then((response) => {
                const { genres } = response.data;
                dispatch({
                    type: actionTypes.FETCH_GENRE_RECORDS_SUCCESS,
                    genres,
                });
                return genres;
            })
            .catch(({ error }) => {
                dispatch({ type: actionTypes.FETCH_RECORDS_FAILED, error });
            });
    };
};

export const searchPerson = (searchQuery, searchSettings) => (dispatch) => {
    loadingFetch();

    return api.personSearch.list(searchQuery, searchSettings)
        .then((response) => {
            const {
                searchType,
                sortType,
            } = searchSettings;

            const { results, page, total_pages, total_results } = response.data;
            const resultArray = results.reduce((item, cur) => {
                return [...item, ...cur.known_for];
            }, []);


            dispatch({
                type: actionTypes.FETCH_RECORDS_SUCCESS,
                searchType,
                searchQuery,
                sortType,
                results: resultArray,
                page,
                total_pages,
                total_results,
            });
            return resultArray;
        })
        .catch(({ error }) => {
        debugger
            dispatch({ type: actionTypes.FETCH_RECORDS_FAILED, error });
        });
};
