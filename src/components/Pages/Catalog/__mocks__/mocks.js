export const searchResponseMock = {
                                 page: 1,
                                 total_results: 12,
                                 total_pages: 1,
                                 searchQuery: "KILL BILL",
                                 searchType: "title",
                                 sortType: "rating",
                                 results: [
                                   {
                                     vote_count: 5381,
                                     id: 24,
                                     video: false,
                                     vote_average: 7.8,
                                     title: "Kill Bill: Vol. 1",
                                     popularity: 21.807171,
                                     poster_path: "/97fNAi62HawGjWru7PvVmF7RAbU.jpg",
                                     original_language: "en",
                                     original_title: "Kill Bill: Vol. 1",
                                     genre_ids: [
                                       28,
                                       80
                                     ],
                                     backdrop_path: "/kkS8PKa8c134vXsj2fQkNqOaCXU.jpg",
                                     adult: false,
                                     overview: "An assassin is shot at the altar by her ruthless employer, Bill and other members of their assassination circle – but 'The Bride' lives to plot her vengeance. Setting out for some payback, she makes a death list and hunts down those who wronged her, saving Bill for last.",
                                     release_date: "2003-10-10"
                                   },
                                 ]
                               };


