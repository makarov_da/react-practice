import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Header from 'common/Header';
import Content from 'common/Content';
import Footer from 'common/Footer';

import { Redirect, withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as actions from './actions';

import styles from './style.less';

const propTypes = {
    searchType: PropTypes.string.isRequired,
    sortType: PropTypes.string.isRequired,
    searchQuery: PropTypes.string,
    results: PropTypes.array,
    genres: PropTypes.array,
    totalResults: PropTypes.number,

    match: PropTypes.shape({
        params: PropTypes.shape({
            filmId: PropTypes.string,
        }),
    }),

    setSearchType: PropTypes.func.isRequired,
    setSortType: PropTypes.func.isRequired,
    setSearchQuery: PropTypes.func.isRequired,
    searchFilms: PropTypes.func.isRequired,
    searchPerson: PropTypes.func.isRequired,
    initializeCatalog: PropTypes.func.isRequired,
    getGenre: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
};

class Catalog extends Component {
    componentDidMount() {
        const {
            initializeCatalog,
            getGenre,
            genres,
        } = this.props;

        initializeCatalog();

        if (!genres || !genres.length) {
            getGenre();
        }
    }
    handleSearchType = (e) => {
        e.preventDefault();
        e.stopPropagation();

        const {
            sortType,
            setSearchType,
            searchQuery,
            searchPerson,
            searchFilms,
            searchType,
        } = this.props;


        const { innerText } = e.target;

        if (searchQuery) {
            (innerText.toLowerCase() === 'director')
                ? searchPerson(searchQuery, {
                    sortType,
                    searchType: innerText.toLowerCase(),
                })
                : searchFilms(searchQuery, {
                    sortType,
                    searchType: innerText.toLowerCase(),
                });
        }
        if (!searchQuery && innerText && innerText !== searchType) setSearchType(innerText.toLowerCase());
    };

    handleSort = (e) => {
        e.preventDefault();
        e.stopPropagation();

        const {
            sortType,
            setSortType,
            searchQuery,
            searchFilms,
            searchPerson,
            searchType,
        } = this.props;
        const { innerText } = e.target;

        if (searchQuery) {
            (searchType.toLowerCase() === 'director')
                ? searchPerson(searchQuery, {
                    searchType,
                    sortType: innerText.toLowerCase(),
                })
                : searchFilms(searchQuery, {
                    searchType,
                    sortType: innerText.toLowerCase(),
                });
        }

        if (!searchQuery && innerText && innerText !== sortType) setSortType(innerText.toLowerCase());
    };

    handleInputChange = (e) => {
        e.preventDefault();
        e.stopPropagation();

        const {
            setSearchQuery,
        } = this.props;

        const { value } = e.target;
        setSearchQuery(value);
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {
            searchType,
            sortType,
            searchQuery,
            searchFilms,
            searchPerson,
        } = this.props;

        if (searchQuery) {
            searchType === 'director'
                ? searchPerson(searchQuery, {
                    searchType,
                    sortType,
                })
                : searchFilms(searchQuery, {
                    searchType,
                    sortType,
                });
        }
    };

    render() {
        const {
            searchType,
            sortType,
            searchQuery,

            isLoading,

            results,
            totalResults,
            match,
            genres,
        } = this.props;

        return <section className={styles.root}>
            <Header
                filmsCount={totalResults}
                searchType={searchType}
                sortType={sortType}
                totalResults={totalResults}
                searchQuery={searchQuery}
                handleInputChange={this.handleInputChange}
                handleSubmit={this.handleSubmit}
                handleSort={this.handleSort}
                handleSearchType={this.handleSearchType}
                match={match}
            />
            <Content
                films={results}
                isLoading={isLoading}
                genres={genres}
            />
            <Footer />
            {
                searchQuery && isLoading &&
                <Redirect to={`/search/${searchQuery}`} />
            }
        </section>;
    }
}

Catalog.propTypes = propTypes;

export default withRouter(connect(state => ({
    searchType: state.catalog.get('searchType'),
    sortType: state.catalog.get('sortType'),
    searchQuery: state.catalog.get('searchQuery'),

    isLoading: state.catalog.get('isLoading'),

    results: state.catalog.get('results').toJS(),
    genres: state.catalog.get('genres').toJS(),
    page: state.catalog.get('page'),
    totalPages: state.catalog.get('total_pages'),
    totalResults: state.catalog.get('total_results'),

}), actions)(Catalog));