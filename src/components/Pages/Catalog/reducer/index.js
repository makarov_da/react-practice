import Immutable from 'immutable';
import * as actionType from '../actions/actionTypes';

const initialState = Immutable.fromJS({
    initPageCatalog: false,
    searchType: 'title',
    sortType: 'rating',
    searchQuery: '',
    isLoading: false,
    results: [],
    genres: [],
    page: 0,
    total_pages: 0,
    total_results: 0,
});

export const catalogPageReducer = (state = initialState, action) => {
    switch (action.type) {
    case actionType.INITIALIZE:
        return state.set('initPageCatalog', true);
        break;

    case actionType.SEARCH_QUERY:
        return state.set('searchQuery', action.searchQuery);
        break;

    case actionType.TYPE_SEARCH:
        return state.set('searchType', action.searchType);
        break;

    case actionType.TYPE_SORT:
        return state.set('sortType', action.sortType);
        break;

    case actionType.FETCH_RECORDS_LOADING:
        return state.set('isLoading', true);
        break;
    case actionType.FETCH_GENRE_RECORDS_SUCCESS:
        return state.merge({
            isLoading: false,
            genres: action.genres,
        });
        break;
    case actionType.FETCH_RECORDS_SUCCESS:
        return state.merge({
            searchType: action.searchType,
            sortType: action.sortType,
            searchQuery: action.searchQuery,
            isLoading: false,
            results: action.results,
            page: action.page,
            total_pages: action.total_pages,
            total_results: action.total_results,
        });
        break;
    case actionType.FETCH_RECORDS_FAILED:
        break;
    default:
        return state;
    }
};