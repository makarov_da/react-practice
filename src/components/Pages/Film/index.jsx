import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from 'common/Header';
import Content from 'common/Content';
import Footer from 'common/Footer';

import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as actions from './actions';

import styles from './style.less';

const propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            filmId: PropTypes.string,
        }),
    }),
    getFilmInfo: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    cast: PropTypes.array,
    filmInfo: PropTypes.object,
    director: PropTypes.string,
    knownFor: PropTypes.array,
};

class Film extends Component {
    componentDidMount() {
        const {
            getFilmInfo,
            match,
        } = this.props;
        getFilmInfo(match.params.filmId);
    }

    render() {
        const {
            match,
            isLoading,
            filmInfo,
            cast,
            director,
            knownFor,
        } = this.props;

        return (
            <section className={styles.root}>
                <Header
                    match={match}
                    filmInfo={filmInfo}
                    cast={cast}
                    director={director}
                />
                {
                    !isLoading && <Content films={knownFor} />
                }
                <Footer />
            </section>
        );
    }
}
Film.propTypes = propTypes;

export default withRouter(connect(state => ({
    isLoading: state.film.get('isLoading'),
    filmInfo: state.film.get('data').toJS(),
    cast: state.film.get('cast').toJS(),
    knownFor: state.film.get('knownFor').toJS(),
    director: state.film.get('director'),
}), actions)(Film));