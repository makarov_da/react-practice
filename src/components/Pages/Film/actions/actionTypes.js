const prefix = '@@film/';

export const INITIALIZE = `${prefix}INIT`;

export const FETCH_RECORDS_LOADING = `${prefix}FETCH_START`;
export const FETCH_RECORDS_SUCCESS = `${prefix}FETCH_SUCCESS`;
export const FETCH_RECORDS_FAILED = `${prefix}FETCH_FAILED`;
