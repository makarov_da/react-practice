import api from 'common/api';
import * as actionTypes from './actionTypes';

export function initializeFilmPage() {
    return {
        type: actionTypes.INITIALIZE,
    };
}


export function getFilmInfo(filmId) {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_RECORDS_LOADING });

        const promise = Promise.all([
            api.filmInfo.list(filmId),
            api.filmInfo.credits(filmId),
        ]);

        promise.then((resp) => {
            const { data } = resp[0];
            const { crew, cast } = resp[1].data;
            const director = crew.filter(person => person.job.toLowerCase() === 'director');
            return api.personSearch.list(director[0].name, { sortType: 'rating' })
                .then((response) => {
                    const { known_for } = response.data.results[0] || [];

                    return dispatch({
                        type: actionTypes.FETCH_RECORDS_SUCCESS,
                        knownFor: known_for,
                        cast,
                        data,
                        director: director[0].name,
                    });
                });
        });
    };
}