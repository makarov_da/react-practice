import Immutable from 'immutable';
import { filmPageReducer } from '../reducer';
import * as actionTypes from '../actions/actionTypes';

describe('filmPageReducer reducer', () => {

    it('should return the initial state', () => {
        expect(filmPageReducer(undefined, {})).toEqual(Immutable.fromJS({
                isLoading: true,
                data: {},
                knownFor: [],
                cast: [],
                director: '',
            })
        )
    });

    it('should return getFilm', () => {
        expect(filmPageReducer(undefined,
            {
                data: [{key: 'value'}],
                knownFor: [{key: 'value'}],
                cast: [{key: 'value'}],
                director: 'Tarantino',
                isLoading: true,
                type: actionTypes.FETCH_RECORDS_SUCCESS
            }))
            .toEqual(Immutable.fromJS({
                isLoading: false,
                data: [{key: 'value'}],
                knownFor: [{key: 'value'}],
                cast: [{key: 'value'}],
                director: 'Tarantino',
        }))
    });

    it('should return typeSearch', () => {
        expect(filmPageReducer(undefined,{error: 'text', isLoading: false, type: actionTypes.FETCH_RECORDS_FAILED}))
            .toEqual(Immutable.fromJS({
                isLoading: false,
                data: {},
                knownFor: [],
                cast: [],
                director: '',
                error: 'text',
        }))
    });
});