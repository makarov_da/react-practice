import Immutable from 'immutable';
import * as actionType from '../actions/actionTypes';

const initialState = Immutable.fromJS({
    isLoading: true,
    data: {},
    knownFor: [],
    cast: [],
    director: '',
});

export const filmPageReducer = (state = initialState, action) => {
    switch (action.type) {
    case actionType.FETCH_RECORDS_LOADING:
        return state.set('isLoading', true);
        break;
    case actionType.FETCH_RECORDS_SUCCESS:
        return state.merge({
            isLoading: false,
            data: action.data,
            cast: action.cast,
            knownFor: action.knownFor,
            director: action.director,
        });
        break;
    case actionType.FETCH_RECORDS_FAILED:
        return state.merge({
            isLoading: false,
            error: action.error,
        });
        break;
    default:
        return state;
    }
};
