import React from 'react';
import PropTypes from 'prop-types';
import styles from './style.less';

const propTypes = {
    color: PropTypes.string,
    size: PropTypes.number,
    uppercase: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

const Label = ({ color, size, uppercase, children }) => {
    const fontSize = {
        fontSize: `${size}px`,
        textTransform: uppercase ? 'uppercase' : 'inherit',
    };
    return <label htmlFor={'somethink'} className={styles[color]} style={fontSize}>
        {children}
    </label>;
};

Label.propTypes = propTypes;

export default Label;