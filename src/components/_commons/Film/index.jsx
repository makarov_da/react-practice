import React from 'react';
import PropTypes from 'prop-types';
import styles from './style.less';

const propTypes = {
    title: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
    poster: PropTypes.string.isRequired,
    genres: PropTypes.array,
};

const src = 'https://image.tmdb.org/t/p/w300/';

const Film = ({ title, year, poster, genres }) => {
    const img = `${src}/${poster}`;
    return (
        <div className={styles.root}>
            <img alt={'Фильм'} src={img} className={styles.image} />
            <div className={styles.wrapInfo}>
                <div className={styles.listInfo}>
                    <span className={styles.title}>{title}</span>
                    <span className={styles.year}>{year}</span>
                </div>
                <div className={styles.listInfo}>
                    <span className={styles.genre}>
                        {
                            genres.map((genre, i) => `${i ? ' /' : ''} ${genre.name}`)
                        }
                    </span>
                </div>
            </div>
        </div>
    );
};

Film.propTypes = propTypes;

export default Film;