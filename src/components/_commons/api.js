import axios from 'axios';

const API_KEY = '27f7f2db860ae45c1130bf4103d99c79';
const API_BASE = 'http://api.themoviedb.org/3';

const api = () => (
    axios.create({
        baseURL: API_BASE,
        timeout: 10000,
        headers: {
            Accept: 'application/json',
        },
    })
);

const data = {
    collectionSearch: {
        list: (searchQuery, searchSettings) => api().get('/search/movie', {
            params: {
                sort_by: searchSettings.sortType === 'rating' ? 'vote_average.asc' : 'release_date.asc',
                query: searchQuery,
                api_key: API_KEY,
            },
        }),
        genre: () => api().get('/genre/movie/list', {
            params: {
                api_key: API_KEY,
            },
        }),
    },
    personSearch: {
        list: (searchQuery, searchSettings) => api().get('search/person', {
            params: {
                sort_by: searchSettings.sortType === 'rating' ? 'vote_average.asc' : 'release_date.asc',
                query: searchQuery,
                api_key: API_KEY,
            },
        }),
    },
    filmInfo: {
        list: id => api().get(`/movie/${id}`, {
            params: {
                api_key: API_KEY,
            },
        }),
        credits: id => api().get(`/movie/${id}/credits`, {
            params: {
                api_key: API_KEY,
            },
        }),
    },
};

export default data;
