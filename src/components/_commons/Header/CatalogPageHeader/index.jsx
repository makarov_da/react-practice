import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Label from 'common/Label';
import Button from 'common/Button';
import SearchField from 'common/SearchField';
import styles from './style.less';

const propTypes = {
    handleInputChange: PropTypes.func,
    handleSubmit: PropTypes.func,
    handleSearchType: PropTypes.func,
    searchType: PropTypes.string,
    valueInput: PropTypes.string,
};

const CatalogPageHeader = ({ handleInputChange, handleSubmit, handleSearchType, searchType, valueInput }) => (
    <section className={styles.root}>
        <div className={styles.infoCatalog}>
            <section className={styles.top}>
                <Link to={'/'}>
                    <Label color={'pink'} size={20}>netfixroulette</Label>
                </Link>
            </section>

            <section className={styles.middleCatalog}>
                <form onSubmit={handleSubmit} className={styles.form}>
                    <div className={styles.searchWrapCatalog}>
                        <Label color={'white'} size={20} uppercase>FIND YOUR MOVIE</Label>
                        <SearchField
                            handleInputChange={handleInputChange}
                            valueInput={valueInput}
                            searchType={searchType}
                        />
                    </div>
                    <div className={styles.linksWrapCatalog}>
                        <Label color={'white'} size={20} uppercase>SEARCH BY</Label>
                        <div className={styles.buttonsCatalog}>
                            <Button
                                color={searchType !== 'title' ? 'grey' : 'pink'}
                                onClickHandler={handleSearchType}
                            >
                                TITLE
                            </Button>
                        </div>
                        <div className={styles.buttonsCatalog}>
                            <Button
                                color={searchType !== 'director' ? 'grey' : 'pink'}
                                onClickHandler={handleSearchType}
                            >
                                DIRECTOR
                            </Button>
                        </div>
                        <div className={styles.searchButton}>
                            <Button type={'submit'} big>SEARCH</Button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
        <div className={styles.overRoot} />
    </section>
);

CatalogPageHeader.propTypes = propTypes;

export default CatalogPageHeader;