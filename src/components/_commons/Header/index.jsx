import React from 'react';
import PropTypes from 'prop-types';

import FilmPageHeader from './FilmPageHeader';
import CatalogPageHeader from './CatalogPageHeader';
import Filter from '../Filter';
import styles from './style.less';

const propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            filmId: PropTypes.string,
        }),
    }),

    totalResults: PropTypes.number,

    filmInfo: PropTypes.object,
    cast: PropTypes.array,
    director: PropTypes.string,

    searchType: PropTypes.string,
    sortType: PropTypes.string,
    searchQuery: PropTypes.string,
    handleInputChange: PropTypes.func,
    handleSubmit: PropTypes.func,
    handleSort: PropTypes.func,
    handleSearchType: PropTypes.func,
};

const Header = ({ searchType, sortType, searchQuery, handleInputChange, handleSubmit, handleSearchType, totalResults,
    handleSort, filmInfo = '', cast, director, match }) => {
    const { filmId } = match.params;

    return (
        <section>
            {
                filmId
                    ? <FilmPageHeader
                        filmInfo={filmInfo}
                        cast={cast}
                        director={director}
                    />
                    : <CatalogPageHeader
                        handleInputChange={handleInputChange}
                        handleSubmit={handleSubmit}
                        handleSearchType={handleSearchType}
                        searchType={searchType}
                        valueInput={searchQuery}
                    />
            }
            <section className={styles.filter}>
                {
                    filmId
                        ? <div>
                            Films by {director}
                        </div>
                        : !!totalResults
                        && <Filter sortType={sortType} handleSort={handleSort} totalResults={totalResults} />
                }
            </section>
        </section>
    );
};

Header.propTypes = propTypes;

export default Header;