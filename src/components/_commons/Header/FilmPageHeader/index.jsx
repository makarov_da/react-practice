import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Label from 'common/Label';
import Button from 'common/Button';
import styles from './style.less';


const propTypes = {
    cast: PropTypes.array,
    filmInfo: PropTypes.object,
    director: PropTypes.string,
};

const FilmPageHeader = ({ filmInfo, cast, director }) => {
    const {
        title,
        genres,
        release_date,
        runtime,
        overview,
        poster_path,
    } = filmInfo;

    const src = 'https://image.tmdb.org/t/p/w300/';
    const date = release_date && release_date.split('-');

    return <section className={styles.root}>
        <div className={styles.wrapTop}>
            <section className={styles.top}>
                <div className={styles.logoLink}>
                    <Link to={'/'}>
                        <Label color={'pink'} size={20}>netfixroulette</Label>
                    </Link>
                </div>
                <div className={styles.searchButton}>
                    <Link to={'/'}>
                        <Button color={'white'}>SEARCH</Button>
                    </Link>
                </div>
            </section>
            <section className={styles.filmInformation}>
                <div className={styles.filmCover}>
                    <img className={styles.image} src={`${src}${poster_path}`} alt={''} />
                </div>
                <div className={styles.filmDescription}>
                    <h2 className={styles.filmTitle}>{title}</h2>
                    <h5>{genres && genres.map(({ name }) => `${name} `)}</h5>
                    <h6 className={styles.numberFilmInformation}>{date && date[0]} {runtime} min</h6>
                    <p>
                        {overview}
                    </p>
                    <p className={styles.subFilmInformation}>
                        Director: {director}
                    </p>
                    <p className={styles.subFilmInformation}>
                        Cast:
                        {
                            cast.map((actor, index) => (
                                <span key={index}>
                                    {`${index ? ', ' : ''} ${actor.name}`}
                                </span>
                            ))
                        }
                    </p>
                </div>
            </section>
        </div>
        <div className={styles.overRoot} />
    </section>;
};
FilmPageHeader.propTypes = propTypes;

export default FilmPageHeader;