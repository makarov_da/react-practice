import React from 'react';
import PropTypes from 'prop-types';
import styles from './style.less';

const propTypes = {
    color: PropTypes.string,
    big: PropTypes.bool,
    type: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    onClickHandler: PropTypes.func,
};

const Button = ({ color = 'pink', big = false, type = 'button', onClickHandler, children }) => {
    const padding = {
        padding: big ? '13px 68px' : '8px 20px',
    };
    return <button
        onClick={onClickHandler}
        className={styles[color]}
        style={padding}
        type={type}
    >
        {children}
    </button>;
};

Button.propTypes = propTypes;

export default Button;