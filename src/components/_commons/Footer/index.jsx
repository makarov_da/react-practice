import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Label from '../Label';
import styles from './style.less';

const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

const Footer = ({ children }) => (
    <section className={styles.root}>
        <div className={styles.overRoot} />
        <div className={styles.inner}>
            <Link to={'/'}>
                <Label color={'pink'} size={20}>netfixroulette</Label>
            </Link>
        </div>
        {children}
    </section>
);

Footer.propTypes = propTypes;

export default Footer;
