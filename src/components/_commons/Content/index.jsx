import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Film from '../Film';
import styles from './style.less';

const propTypes = {
    films: PropTypes.array,
    genres: PropTypes.array,
    isLoading: PropTypes.bool,
};

const Content = ({ films, genres, isLoading }) => {
    const catalogFilms = films && films.length && films.map((film, i) => {
        if (film.title && film.release_date && film.poster_path) {
            const link = `/film/${film.id}/`;
            return (
                <div key={i} className={styles.wrapFilms}>
                    <Link to={link}>
                        <Film
                            title={film.title}
                            year={film.release_date}
                            poster={film.poster_path}
                            genres={getGenres(film, genres)}
                        />
                    </Link>
                </div>
            );
        }

        return null;
    });


    return <section className={styles.root}>
        {
            isLoading && 'Loading...'
        }
        {
            (!isLoading && catalogFilms) || <h1 className={styles.noFoundText}>No films found</h1>
        }
    </section>;
};

Content.propTypes = propTypes;

export default Content;

const getGenres = (film, genres) => {
    if (genres && genres.length > 0) {
        return genres.filter(genre => film.genre_ids.some(id => id === genre.id));
    }
    return [];
};
