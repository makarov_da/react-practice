import React from 'react';
import PropTypes from 'prop-types';
import styles from './style.less';

const propTypes = {
    sortType: PropTypes.string.isRequired,
    handleSort: PropTypes.func.isRequired,
    totalResults: PropTypes.number,
};

const Filter = ({ sortType, handleSort, totalResults }) => (
    <div className={styles.root}>
        <div>
            {
                `${totalResults} ${totalResults < 1 ? 'movies' : 'movie'} found`
            }
        </div>
        <div>
            Sort by
            <span onClick={handleSort} className={sortType !== 'release date' ? styles.sort : styles.active}>
                release date
            </span>
            <span onClick={handleSort} className={sortType !== 'rating' ? styles.sort : styles.active}>rating</span>
        </div>
    </div>
);

Filter.propTypes = propTypes;

export default Filter;
