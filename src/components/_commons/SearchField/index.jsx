import React from 'react';
import PropTypes from 'prop-types';
import styles from './style.less';

const propTypes = {
    valueInput: PropTypes.string,
    searchType: PropTypes.string,
    handleInputChange: PropTypes.func,
};

const SearchField = ({ valueInput, handleInputChange, searchType }) => <div className={styles.root}>
    <input
        type={'text'}
        value={valueInput}
        onChange={handleInputChange}
        className={styles.searchField}
        placeholder={searchType === 'director' ? 'Quentin Tarantino' : 'KILL BILL'}
    />
</div>;

SearchField.propTypes = propTypes;

export default SearchField;