import React from 'react';
import { render } from 'react-dom';

import configureStore from './store';

import Netflix from './Netflix';
import './favicon.ico';

const store = configureStore();

render(<Netflix store={store} />, document.getElementById('root'));