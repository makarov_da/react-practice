import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Catalog from './components/Pages/Catalog';
import Film from './components/Pages/Film';

const propTypes = {
    store: PropTypes.object,
};

const Netflix = ({ store }) => {
    return (
        <Provider store={store}>
            <Router>
                <div>
                    <Route exact path="/" component={Catalog} />
                    <Route path="/film/:filmId" component={Film} />
                    <Route path="/search/:searchQuery" component={Catalog} />
                </div>
            </Router>
        </Provider>
    );
};

Netflix.propTypes = propTypes;

export default Netflix;