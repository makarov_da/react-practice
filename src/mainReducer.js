import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { catalogPageReducer } from './components/Pages/Catalog/reducer';
import { filmPageReducer } from './components/Pages/Film/reducer';

const mainReducer = combineReducers({
    routing: routerReducer,
    catalog: catalogPageReducer,
    film: filmPageReducer,
});


export default mainReducer;
