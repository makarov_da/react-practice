import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import mainReducer from './mainReducer';

const configureStore = () => {
    const store = createStore(
        mainReducer,
        {},
        compose(
            applyMiddleware(thunk),
            window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        ),
    );
    return store;
};

export default configureStore;
