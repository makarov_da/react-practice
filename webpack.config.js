const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        index: './index',
    },
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js',
    },
    resolve: {
        extensions: ['.js', '.jsx', 'less'],
        alias: {
            'common-styles': path.resolve(__dirname, 'src', 'components/_commons/styles'),
            common: path.resolve(__dirname, 'src', 'components/_commons'),
        },
    },
    watch: true,
    plugins: [
        new ExtractTextPlugin({ filename: 'style.css', allChunks: true }),
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react'],
                        plugins: [
                            'babel-plugin-transform-object-rest-spread',
                            'transform-class-properties',
                        ],
                    },
                },
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            localIdentName: '[folder]---[name]---[local]---[hash:base64:5]',
                            modules: true,
                            importLoaders: 1,
                        },
                    },
                    'less-loader',
                ],
            },
            {
                test: /\.(ico|png|jpg|jpeg|gif)$/,
                use: ['url-loader?limit=60000'],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
};